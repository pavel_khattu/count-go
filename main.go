package main

import (
	"bufio"
	"flag"
	"fmt"
	"os"
	"sync"

	"count-go/counter"
)

type Counter func(source, word string) (int, error)

const (
	searchWord = "Go"
	concReq    = 5 // requests made concurrently
)

// makes sure no more than concReq requests are in work
var sema = make(chan struct{}, concReq)

var typeFlag = flag.String("type", "", "'file' or 'url'")

func main() {
	flag.Parse()
	if typeFlag == nil || *typeFlag == "" {
		fmt.Printf("missing flag '-type'\n")
		flag.Usage()
		os.Exit(1)
	}

	out := make(chan result)
	wg := sync.WaitGroup{}

	var c Counter
	switch *typeFlag {
	case "url":
		c = counter.HtmlCounter
	case "file":
		c = counter.FileCounter
	default:
		fmt.Printf("Unknown type:%s\n", *typeFlag)
		flag.Usage()
		os.Exit(1)
	}

	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		go find(c, scanner.Text(), out)
		wg.Add(1)
	}

	go func() {
		wg.Wait()
		close(out)
	}()

	var total int
	for c := range out {
		if c.err != nil {
			fmt.Printf("Error for %s occured: %s\n", c.source, c.err.Error())
		}
		fmt.Printf("Count for %s: %d\n", c.source, c.count)
		total += c.count
		wg.Done()
	}

	fmt.Printf("Total: %d\n", total)
}

type result struct {
	count  int
	err    error
	source string
}

func find(c Counter, source string, out chan<- result) {
	//acquire token
	sema <- struct{}{}
	//release token
	defer func() { <-sema }()

	count, err := c(source, searchWord)
	out <- result{
		count:  count,
		err:    err,
		source: source,
	}
}
