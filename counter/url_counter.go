package counter

import (
	"bufio"
	"net/http"
	"strings"
)

func HtmlCounter(url, searchWord string)  (total int, err error) {
	resp, err := http.Get(url)
	if err != nil {
		return
	}
	defer resp.Body.Close()

	scanner := bufio.NewScanner(resp.Body)

	for scanner.Scan() {
		total += strings.Count(scanner.Text(), searchWord)
	}

	err = scanner.Err()
	return
}
