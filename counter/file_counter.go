package counter

import (
	"bufio"
	"os"
	"strings"
)

func FileCounter(path, searchWord string) (total int, err error) {
	file, err := os.Open(path)
	if err != nil {
		return
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)

	for scanner.Scan() {
		total += strings.Count(scanner.Text(), searchWord)
	}

	err = scanner.Err()
	return
}
