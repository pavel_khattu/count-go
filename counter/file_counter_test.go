package counter

import "testing"

func TestUrlCounter(t *testing.T) {
	count, err := FileCounter("../fixtures/test", "Go")
	if err != nil {
		t.Errorf("unexpected error: %s", err.Error())
		t.Fail()
	}
	if count != 3 {
		t.Errorf("count isn't 3, but: %d", count)
		t.Fail()
	}
}
