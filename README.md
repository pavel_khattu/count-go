Application for counting 'Go' in files or web pages

To count Go in links: 

`echo -e 'https://golang.org\nhttps://golang.org\nhttps://golang.org' | go run main.go -type url`

To count in files:

`echo -e 'fixtures/test' | go run main.go -type file`